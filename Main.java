public class Main {
    public static void main(String[] args) {
        // Create a car with model, color, engine type, trunk length, trunk breadth and trunk height
        Car myCar = new Car("Sedan", "White", "Petrol", 50, 20, 20);
        System.out.println("Model: "); // Print model to console.
        System.out.println("Colour: "); // Print model to console.
        System.out.println("Engine Type: "); // Print model to console.

        // Change the color
        // Call method to change the color
        System.out.println("Updated Colour: "); // Call the method to retrieve color.

        // Check available volume after placing a luggage of dimensions 20x20x20
        double availableVolume = myCar.getAvailableVolume(20, 20, 20);
        System.out.println("Available Volume after placing luggage: " + availableVolume + " cubic units");
    }
}